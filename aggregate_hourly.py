from pyspark import SparkContext
from pyspark.sql import HiveContext
import pyspark.sql.functions as F

# Stage 2
# Data aggregation script for electricity data upload on Coudera Quickstart VM on Docker.
# Aggregation by hour.

TABLE = "elec_data_AT_IT"

sc = SparkContext(appName="aggregator")
sc.setLogLevel("WARN")
# spark = SparkSession(sc)  # for local dev
sqlContext = HiveContext(sc)

df = sqlContext.table(TABLE)
df = df.withColumn('total_per_s', df.appl1 + df.appl2 + df.appl3 + df.appl4 + df.appl5 + df.appl6 + df.appl7 + df.appl8)

total_df = df.select(df.timestamp_1s, df.building, df.country, df.total_per_s)

df_min = total_df.withColumn('min', F.date_format(F.col('timestamp_1s'), 'yyyy-MM-dd HH:mm'))\
    .groupBy('min', "building")\
    .agg(F.mean(F.col('total_per_s')).alias("average_per_min"))

df_h = df_min.withColumn('hour', F.date_format(F.col('min'), 'yyyy-MM-dd HH'))\
    .groupBy('hour', "building")\
    .agg(F.mean(F.col('average_per_min')).alias("average_per_h"))

df_h.show()

df_h.write.format("orc").saveAsTable("elec_AT_IT_hourly")

