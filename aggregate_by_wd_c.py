from pyspark import SparkContext
from pyspark.sql import HiveContext
import pyspark.sql.functions as F

# Stage 4
# Data aggregation script for electricity data upload on Coudera Quickstart VM on Docker.
# Aggregation by weekday and final comparison by country.

TABLE = "elec_AT_IT_daily_total_kWh"

sc = SparkContext(appName="aggregator")
sc.setLogLevel("WARN")
# spark = SparkSession(sc)  # for local dev
sqlContext = HiveContext(sc)

sqlContext.sql("show tables").show()

df = sqlContext.table(TABLE)

df_day = df.withColumn('weekday', F.date_format(F.col('day'), 'EEEE'))\
    .groupBy('weekday', "building")\
    .agg(F.mean(F.col('total_per_day_kWh')).alias("average_per_day_kWh"))

df_avg_day_b = df_day.groupBy("building", "weekday") \
    .agg(F.mean(F.col('average_per_day_kWh')).alias("average_per_day_kWh"))


df_avg_day_b = df_avg_day_b.withColumn("building", F.substring(F.col("building"), -1, 1).cast('int'))

df_avg_day_c = df_avg_day_b.withColumn("country", F.when(F.col("building") <= 4, "Austria").otherwise("Italy"))

df_avg_day_c= df_avg_day_c.na.drop()

df_avg_day_c.write.format("orc").saveAsTable("elec_AT_IT_weekday_average_kWh")

df_avg_wd_c = df_avg_day_c.groupBy("country", "weekday") \
    .agg(F.mean(F.col('average_per_day_kWh')).alias("average_per_day_kWh"))

df_avg_wd_c = df_avg_wd_c.sort("average_per_day_kWh", ascending=False)

df_avg_wd_c.show()
