##### Electricity Data Processing on Cloudera Quickstart VM on Docker

###### About

This is a proof of concept application to process electricity consumption data
from Austria and Italy using Cloudera Quickstart VM on Docker.

The code is uploading about 17 GB of electricity consumption
data to Hive tables on Cloudera Quickstart VM on Docker using
Pyspark. The data measured by 1 s intervals is aggregated and the final result is a table showing
average electricity consumption in a household by day of the week and by country.

Detailed running instructions are described in the report sent by email.

Developer email:
tomaskazemekas@gmail.com
