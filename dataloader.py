import os
from pyspark import SparkContext
# from pyspark.sql import SparkSession
from pyspark import SparkConf
from pyspark.sql import HiveContext
from pyspark.sql.types import *
from pyspark.sql import Row
from pyspark.sql.functions import lit, from_unixtime, col

# Stage 1.
# Data upload script for electricity data upload on Coudera Quickstart VM on Docker.

DATA_DIR = "/src/GREEND_0-2_300615"  # path on Docker Couldera Quickstart VM
ENDING = "csv"
TABLE = "elec_data_AT_IT"
PROC_COUNT = 0

sc = SparkContext(appName="dataloader")
sc.setLogLevel("WARN")
# spark = SparkSession(sc)  # for local dev
sqlContext = HiveContext(sc)


# Traversing the last data dir to find all data files.
# Return list element structure: (file).
def find_files(data_dir, ending):
    files_list = []
    for root, dirs, files in os.walk(data_dir):
        dirs[:] = [d for d in dirs]
        for f in files:
            if f.endswith(ending):
                files_list.append(os.path.join(root, f))
    return files_list


def get_location(file):
    building = file.split("/")[-2]
    country = "Austria" if int(building[-1]) <= 4 else "Italy"
    return building, country


def ingest_csv(file):
    file_path = "file://" + file
    csv_data = sc.textFile(file_path)
    try:
        csv_data_split = csv_data.map(lambda p: p.split(",")[:9])
    except IndexError:
        return None
    try:
        test_df = csv_data_split.toDF()
    except ValueError:
        return None
    if (len(test_df.columns)) != 9:
        return None
    csv_na_filtered = csv_data_split.filter(lambda p: len(p) == 9)
    header_start = "timestamp"
    csv_data_wo_header = csv_na_filtered.filter(lambda p: p[0] != header_start)
    df_csv = csv_data_wo_header.map(lambda p: Row(timestamp_1s=p[0], appl1=(p[1]), appl2=(p[2]), appl3=p[3],
                                                  appl4=p[4], appl5=p[5], appl6=p[6], appl7=p[7],
                                                  appl8=p[8])).toDF()
    df_clean = df_csv.na.replace("NULL", "0.0")
    building, country = get_location(file)
    df_with_b = df_clean.withColumn("building", lit(building))
    df_with_c = df_with_b.withColumn("country", lit(country))
    return df_with_c


def convert_datatypes(df):
    df_with_ts = df.withColumn("timestamp_1s", from_unixtime(col("timestamp_1s"), format='yyyy-MM-dd HH:mm:ss')
                               .cast("timestamp"))
    for col_name in df_with_ts.columns:
        if col_name.startswith("appl"):
            df_typed = df_with_ts.withColumn(col_name, col(col_name).cast('float'))
    return df_typed


def load_data(files, table, proc_count):
    proc, unproc = [], []
    total = len(files)
    unproc_count = total - proc_count
    unproc_files = files[proc_count:]
    print("STATUS Found " + str(unproc_count) + " unprocessed files out of " + str(total) + " data files.")
    for f in unproc_files:
        df_with_c = ingest_csv(f)
        if not df_with_c:
            print("STATUS File: " + f + " is malformed. Adding the file to unprocessed list.")
            unproc.append(f)
            if proc_count > 0:
                proc_count += 1
            continue
        df_typed = convert_datatypes(df_with_c)
        if proc_count == 0:
            df_typed.write.format("orc").saveAsTable(table)
            print("STATUS Success: created " + table + " from " + f)
            proc_count += 1
            proc.append(f)
        else:
            df_typed.write.insertInto(table)
            proc_count += 1
            print("STATUS Success: updated " + table + " from " + f)
            print("STATUS Processed " + str(proc_count) + " files from total " + str(total))
            proc.append(f)
    return proc, unproc


data_files = find_files(DATA_DIR, ENDING)

proc, unproc = load_data(data_files, TABLE, PROC_COUNT)

print("STATUS Final files processed: " + str(len(proc)))
print("STATUS Final files not processed: " + str(len(unproc)))
