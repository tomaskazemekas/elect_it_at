from pyspark import SparkContext
from pyspark.sql import HiveContext
import pyspark.sql.functions as F

# Stage 3
# Data aggregation script for electricity data upload on Coudera Quickstart VM on Docker.
# Aggregation by day.

TABLE = "elec_AT_IT_hourly"

sc = SparkContext(appName="aggregator")
sc.setLogLevel("WARN")
# spark = SparkSession(sc)  # for local dev
sqlContext = HiveContext(sc)

df = sqlContext.table(TABLE)

df_day = df.withColumn('day', F.date_format(F.col('hour'), 'yyyy-MM-dd'))\
    .groupBy('day', "building")\
    .agg(F.sum(F.col('average_per_min')/1000).alias("total_per_day_kWh"))

df_day.show()

df_day.write.format("orc").saveAsTable("elec_AT_IT_daily_total_kWh")
